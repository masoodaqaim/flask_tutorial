from flaskblog import db, login_manager
from flask_login import UserMixin
from datetime import datetime
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from flask import current_app


# this is from flask login_manager documentation
@login_manager.user_loader  # note the lack of ()
def load_user(user_id):
    return User.query.get(int(user_id))


# this User class will be used to create our db. you can create columns here
class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)  # the id column will be the primary key
    username = db.Column(db.String(20), unique=True, nullable=False)  # a string of 20 characters, must be unique, and cannot be null
    email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default='default.jpg')
    password = db.Column(db.String(60), nullable=False)
    posts = db.relationship('Post', backref='author', lazy=True)  # the 'posts' attribute has a relationship to the 'Post' class model

    def get_reset_token(self, expires_sec=1800):
        s = Serializer(current_app.config['SECRET_KEY'], expires_sec)
        return s.dumps({'user_id': self.id}).decode('utf-8')

    @staticmethod  # used to ignore the self argument
    def verify_reset_token(token):
        s = Serializer(current_app.config['SECRET_KEY'])
        try:
            user_id = s.loads(token)['user_id']
        except:
            return None
        return User.query.get(user_id)

    # magic method...not sure what this does
    def __repr__(self):
        return f"User('{self.username}', '{self.email}', '{self.image_file}')"


# post class to hold our posts
class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), nullable=False)
    date_posted = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)  # note, you are passing the function utcnow, not running it with utcnow(). note the parenthesis
    content = db.Column(db.Text, nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)

    def __repr__(self):
        return f"Post('{self.title}', '{self.date_posted}')"