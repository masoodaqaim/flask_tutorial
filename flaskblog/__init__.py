"""
the __init__ file

I have little understanding of what __init__ file is
"""

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mail import Mail
from flaskblog.config import Config


db = SQLAlchemy()
bcrypt = Bcrypt()  # bcrypt will hold the users hashed password
login_manager = LoginManager()
login_manager.login_view = 'users.login'  # this is the function name
login_manager.login_message_category = 'info'
mail = Mail()


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(Config)  # using the Config object to apply the configs to the app. 'Config' is the object which contains all the configurations.

    # the extensions of the app being applied
    db.init_app(app)
    bcrypt.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)

    # importing routes after app, app config, and db have been initialized to avoid circular imports
    from flaskblog.users.routes import users  # the 'users' is the blueprint instance of the users.routes. same is true for 'posts' and 'main'
    from flaskblog.posts.routes import posts
    from flaskblog.main.routes import main

    # here is where the blueprints are being registered
    app.register_blueprint(users)
    app.register_blueprint(posts)
    app.register_blueprint(main)

    return app
