'''
only the routes that are specific to posts functionality will be included in this blueprint
'''
from flask import Blueprint, render_template, url_for, flash, redirect, request, abort
from flask_login import current_user, login_required
from flaskblog import db
from flaskblog.models import Post
from flaskblog.posts.forms import PostForm

posts = Blueprint('posts', __name__)


# note, just like the users/routes.py blueprint, we are not using app.route. we are using posts.route and this will be registered to the application
# route to add new posts
@posts.route("/post/new", methods=['GET', 'POST'])
@login_required  # this is the decorator to require users to be logged in to route to this route
def new_post():
    form = PostForm()  # this creates the instants of the PostForm() class. you then pass this in the render_template
    if form.validate_on_submit():
        post = Post(title=form.title.data, content=form.content.data, author=current_user)
        db.session.add(post)
        db.session.commit()
        flash('Your post has been created.', 'success')  # 'success' is the category
        return redirect(url_for('main.home'))  # redirect them the home page
    return render_template('new_post.html', title='Create a post', form=form, legend='Create post')


# routes the the post webpage. this is dynamic in that with each new post posted, a new /post/<int:post_id> is created
@posts.route("/post/<int:post_id>")
def post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template('post.html', title=post.title, post=post)


@posts.route("/post/<int:post_id>/update", methods=['GET', 'POST'])
@login_required
def update_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    form = PostForm()
    if form.validate_on_submit():
        post.title = form.title.data
        post.content = form.content.data
        db.session.commit()
        flash('Your post has been updated.', 'success')  # a flash message
        return redirect(url_for('posts.post', post_id=post.id))
    elif request.method == 'GET':
        form.title.data = post.title
        form.content.data = post.content
    return render_template('new_post.html', title='Update a post', form=form, legend='Update post')


# this deletes a post. however the post.html file with the bootstrap modal example is not working. not sure why
@posts.route("/post/<int:post_id>/delete", methods=['POST'])
@login_required
def delete_post(post_id):
    post = Post.query.get_or_404(post_id)
    if post.author != current_user:
        abort(403)
    db.session.delete(post)  # this is how to delete from the database which in turn deletes the post
    db.session.commit()
    flash('Your post has been deleted.', 'success')  # a flash message
    return redirect(url_for('main.home'))