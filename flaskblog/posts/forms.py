from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired


# the FlaskForm for creating a post. this will be passed in the return render_template as form=form
# from there, the html page will read and we can use {{}} to return any information passed into these forms
class PostForm(FlaskForm):
    title = StringField('Title', validators=[DataRequired()])
    content = TextAreaField('Content', validators=[DataRequired()])
    submit = SubmitField('Post')