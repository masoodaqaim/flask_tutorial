'''
only the routes that are specific to homepage functionality will be included in this blueprint
'''
from flask import render_template, request, Blueprint
from flaskblog.models import Post

main = Blueprint('main', __name__)


# the @app decorator is the url link to each function
# in the bottom example, two decorators are declared "/" and "/home"
# the user can use either option to navigate back to the home page
@main.route("/")
@main.route("/home")
def home():
    page = request.args.get('page', 1, type=int)  # if users enter anything other than an int for a page, an error appears
    posts = Post.query.order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)  # this pagination will result in having only 5 post per page
    return render_template('home.html', posts=posts)  # the posts parameter can be used to loop through data. the for-loop is in the home.html file


# using "/about" will allow the user to navigate to the about page
@main.route("/about")
def about():
    return render_template('about.html', title='About')