from flask_wtf import FlaskForm
from flask_wtf.file import FileField, FileAllowed
from wtforms import StringField, PasswordField, SubmitField, BooleanField
from wtforms.validators import DataRequired, Length, Email, EqualTo, ValidationError
from flask_login import current_user
from flaskblog.models import User


# below is the class to create a registration form for users. they have to enter a username, email, password, then submit
# the StringFiled is part of wtforms package and it is just a string input; PasswordFiled is a password input, SubmitFiled is a submit button
# the validators parameters are used to ensure the user enters a value that confines to whatever I pass/require. Validators is a list
# note, the EqualTo validator is set to have confirm_password equal to password
class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Sign up')

    # a custom validation function to check a user does not enter an already used username to register. this checks our databased that we have commited into
    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()  # username.data is comming from forms.py. this returns the first instance of a user and checks it against the newly created user
        if user:  # if user = True, then
            raise ValidationError('That username is taken. Please choose a different one.')

    # a custom validation function to check a user does not enter an already used email address to register
    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()  # this returns the first instance of a user and checks it against the newly created user
        if user:
            raise ValidationError('That email is taken. Please choose a different one.')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember = BooleanField('Remember me')
    submit = SubmitField('Sign up')

# class to update user account information
class UpdateAccountForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired(), Length(min=2, max=20)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    picture = FileField('Update Profile Picture', validators=[FileAllowed(['jpg', 'png'])])  # FileAllowed only allows the extentions you pass into it
    submit = SubmitField('Update')

    # a custom validation function to check a user does not enter an already used username to register. this checks our databased that we have commited into
    # only do the validation steps if the username/email they enter, is different from their current username/email. in other words, we do not want the user to change their username/email
    def validate_username(self, username):
        if username.data != current_user.username:
            user = User.query.filter_by(username=username.data).first()  # username.data is comming from forms.py. this returns the first instance of a user and checks it against the newly created user
            if user:  # if user = True, then
                raise ValidationError('That username is taken. Please choose a different one.')

    # a custom validation function to check a user does not enter an already used email address to register
    # same as above except this is for email
    def validate_email(self, email):
        if email.data != current_user.email:
            user = User.query.filter_by(email=email.data).first()  # this returns the first instance of a user and checks it against the newly created user
            if user:
                raise ValidationError('That email is taken. Please choose a different one.')


# a form to submit their account to be rested
class RequestResetForm(FlaskForm):
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Request Password Reset')

    # function to check if the user exist prior to resting the password. cant reset password if the username has not been created
    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()  # this returns the first instance of a user and checks it against the newly created user
        if user is None:
            raise ValidationError('There is not account with that email. Please create an account.')


class ResetPasswordForm(FlaskForm):
    password = PasswordField('Password', validators=[DataRequired()])
    confirm_password = PasswordField('Confirm Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Reset Password')