from flask import render_template, url_for, flash, redirect, request, Blueprint
from flask_login import login_user, current_user, logout_user, login_required
from flaskblog import db, bcrypt
from flaskblog.models import User, Post
from flaskblog.users.forms import RegistrationForm, LoginForm, UpdateAccountForm, RequestResetForm, ResetPasswordForm
from flaskblog.users.utils import save_picture, send_reset_email
'''
only the routes that are specific to user functionality will be included in this blueprint
example: register, login, password reset, etc
'''

users = Blueprint('users', __name__)  #


# not using global 'app' variable to create the routes. creating routes specific to this 'users' blueprint, then register them to the application
@users.route("/register", methods=['GET', 'POST'])  # the methods list allows which type of html request to allow
def register():
    if current_user.is_authenticated:  # this will reroute users to the home page if they are already logged in
        return redirect(url_for('main.home'))
    form = RegistrationForm()

    # the validate_on_submit() method. this creates a user
    if form.validate_on_submit():
        hashed_pw = bcrypt.generate_password_hash(form.password.data).decode('utf-8')  # form.password.data is the user input; this is hashed; it is also outputted as a string with passing of utf-8
        user = User(username=form.username.data, email=form.email.data, password=hashed_pw)  # this creates and stores a new user. NOTE, ONLY pass the hashed_pw NOT the form.password.data as that will capture the plain text entered password
        db.session.add(user)  # adding the newly created user to our database
        db.session.commit()  # committing the newly created user
        flash('Your account has been created.', 'success')  # the 'success' is a bootstrap Class
        return redirect(url_for('users.login'))  # this redirects the user to the login page

    return render_template('register.html', title='Register', form=form)


@users.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('users.home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, form.password.data):  # check if the user exits and if the entered password matches the hash password in the database
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')  # this is only relevant if they navigate directly to the account page without being logged in. this will keep them on the account page instead of navigating them to the home page
            return redirect(next_page) if next_page else redirect(url_for('main.home')) # return the next_page if it exits, else return the homepage
        else:
            flash('Login Unsuccessful. Please check credentials and try again.', 'danger')  # 'danger' is a category for bootstrap as to how the flash() is the be displayed
    return render_template('login.html', title='Login', form=form)


# this will logout the current user
@users.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('main.home'))


# this will allow users to only be able to see content if they are logged
@users.route("/account", methods=['GET', 'POST'])
@login_required  # this decorate tells flask that to navigate to account.html, the user needs to be logged in
def account():
    form = UpdateAccountForm()
    if form.validate_on_submit():
        if form.picture.data:
            picture_file = save_picture(form.picture.data)
            current_user.image_file = picture_file

        current_user.username = form.username.data
        db.session.commit()
        flash('Account has been updated', 'success')
        return redirect(url_for('users.account'))
    elif request.method == 'GET':  # this will populate both forms with the current user's data
        form.username.data = current_user.username
        form.email.data = current_user.email
    # current_user is a flask package; image_file is the variable located in the models.py file
    image_file = url_for('static', filename=f'profile_pic/{current_user.image_file}')  # 'static' is the directory; filename is the actual file name
    return render_template('account.html', title='Account', image_file=image_file, form=form)  # note, you can pass anything in render_template and have that variable be used in the html files


# this will show all and only the post from a specific author; the only clicked on
@users.route("/user/<string:username>")  # the <> makes this dynamic
def user_posts(username):
    page = request.args.get('page', 1, type=int)  # if users enter anything other than an int for a page, an error appears
    user = User.query.filter_by(username=username).first_or_404()
    posts = Post.query.filter_by(author=user).order_by(Post.date_posted.desc()).paginate(page=page, per_page=5)  # this pagination will result in having only 5 post per page
    return render_template('user_posts.html', posts=posts, user=user)  # the posts parameter can be used to loop through data. the for-loop is in the home.html file


# route to request a reset of the password
@users.route("/reset_password", methods=['GET', 'POST'])
def reset_request():
    if current_user.is_authenticated:  # the user must be logged out before resetting their password
        return redirect(url_for('main.home'))
    form = RequestResetForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()
        send_reset_email(user)
        flash('An email has been sent to reset your password.' 'info')
        return redirect(url_for('users.login'))
    return render_template('reset_request.html', title='Reset Password', form=form)


# this will reset their password with the token
@users.route("/reset_password/<token>", methods=['GET', 'POST'])
def reset_token(token):
    if current_user.is_authenticated:  # the user must be logged out before resetting their password
        return redirect(url_for('main.home'))
    user = User.verify_reset_token(token)
    if user is None:  # this will check if the token a user uses has not expired yet
        flash('That token has expired.', 'warning')
        return redirect(url_for('users.reset_request'))
    form = ResetPasswordForm()

    if form.validate_on_submit():
        hashed_pw = bcrypt.generate_password_hash(form.password.data).decode('utf-8')  # form.password.data is the user input; this is hashed; it is also outputted as a string with passing of utf-8
        user.password = hashed_pw
        db.session.commit()  # committing the changes
        flash('Your password has been updated.', 'success')  # the 'success' is a bootstrap Class
        return redirect(url_for('users.login'))  # this redirects the user to the login page

    return render_template('reset_token.html', title='Reset Password', form=form)