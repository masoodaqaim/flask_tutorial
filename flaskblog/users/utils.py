import os
import secrets
from PIL import Image
from flask import url_for, current_app
from flask_mail import Message
from flaskblog import mail
'''
only routes that are utility related (ie saving a pic), will be included in this blueprint
'''


# function save a pic from your local machine and capture the root path and the extension
def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    f_name, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(current_app.root_path, 'static/profile_pic', picture_fn)

    # this resizes the image to 125x125 pixels. reason is for performance
    output_size = (125, 125)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn


# function to send a reset password email
def send_reset_email(user):
    token = user.get_reset_token()
    msg = Message('Password Reset Request', sender='noreply@demo.com', recipients=[user.email])
    # note, the _external arg is to get the absolute url
    msg.body = f'''To reset your password, visit on the following link (link will expire in 30 mins):
    {url_for('users.reset_token', token=token, _external=True)}

    If you did not make this request, ignore this email and no changes will be made.
    '''

    mail.send(msg)