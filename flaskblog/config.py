import os


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY')  # both the secret key and sqlalchemy db are environment variables to mask the info
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI')
    MAIL_SERVER = 'smtp.googlemail.com'
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = os.environ.get('EMAIL_USER')  # this is an environment variable used to mask sensitive information
    MAIL_PASSWORD = os.environ.get('EMAIL_PASS')  # this is an environment variable used to mask sensitive information